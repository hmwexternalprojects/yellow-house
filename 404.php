<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _s
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main four-oh-four__wrapper">

    <section class="bmcb-section container error-404 not-found text-center">
      <header class="py-8">
        <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'hmw' ); ?></h1>
      </header><!-- .page-header -->

      <div class="page-content row bmcb-row flex flex-col items-center">
        <p>
          <?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'hmw' ); ?>
        </p>

        <?php
					get_search_form();

					// the_widget( 'WP_Widget_Recent_Posts' );
					?>
      </div><!-- .page-content -->
    </section><!-- .error-404 -->

  </main><!-- #main -->
</div><!-- #primary -->

<?php
echo render_common_globals();
get_footer();
