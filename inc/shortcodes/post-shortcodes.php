<?php

// [testi-slider] You can use any JS slider library you like. Default is Siema
add_shortcode('testi-slider', function($atts) {
	$atts = shortcode_atts( 
        array(
			'perpage' => 2,
			'image' => true,
			'width' => ''
        ), 
        $atts);

	$args = array(
        'post_type' => 'testimonials',
        'posts_per_page' => $atts['perpage'],
        'post_status' => 'publish'
    );

	$query = new WP_Query($args);

	ob_start();
	
	?>

<div class="my-slider testimonial-slider">
  <?php if ($query->have_posts()) : ?>
  <?php while($query->have_posts()) : $query->the_post(); 
				$ID = get_the_id();
				$width = $atts['width'];
				$imageEnabled = $atts['image'] !== 'false';
        $image = get_the_post_thumbnail_url($ID,'full'); // Returns URL of any custom size (obv)
        $body = wp_trim_words(get_the_content(), 40);
        if (function_exists('get_field')) :
          $name = get_field('name') ?: null;
          $company = get_field('company_name') ?: null;
        endif;
			?>
  <div
    class="testimonial-slider__slide <?php echo ($width) ? "is-$width" : '' ?> <?php echo !$imageEnabled ? 'image-false' : 'image-true'; ?>"
    <?php echo $imageEnabled ? "style=\"background: url('$image')\"" : ''; ?>>
    <div class="container">
      <div class="testimonial-slider__content-box">
        <div class="testimonial-slider__content">
          <div class="testimonial-slider__body"><?php echo __($body); ?></div>
          <?php if ($name || $company) : ?>
          <div class="testimonial-slider__info flex items-center">
            <div class="testimonial-slider__name"><?php echo __($name); ?></div>
            <div class="testimonial-slider__company ml-2"><?php echo __($company); ?></div>
          </div>
          <?php endif; ?>
        </div>
        <!-- <div class="testimonial-slider__arrow-group">
          <div class="testimonial-slider__arrow arrow-left">
            <i class="fa fa-chevron-left"></i>
          </div>
          <div class="testimonial-slider__arrow arrow-right">
            <i class="fa fa-chevron-right"></i>
          </div>
        </div> -->
      </div>
    </div>
  </div>
  <?php endwhile; ?>
  <?php endif; ?>
</div>

<?php
	$output = ob_get_clean();
    wp_reset_postdata();
    return $output;
});

// This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
add_shortcode('list-posts', function($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts( [
		"perpage" => 6,
		"offset" => 3,
		"cols" => 3,
		"cat" => null,
		"post_type" => 'post',
		"paged" => true,
	], $atts );
	
	if (!get_query_var('categories') && isset($atts['cat'])) {
		$categoryIDs = $atts['cat']; 
	}

	$pagination = ($atts['perpage'] > 6 || $atts['perpage'] === -1);	
	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );

	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	
	$args = [
		"posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => $atts['post_type'],
		"post_status" => "publish",
		"category__in" => $categoryIDs ? [$categoryIDs] : ''
	];

	if ($pagination) {
		$args['paged'] = $current_page;
	}
	
	$query = new WP_Query($args); 

	$total_rows = max( 0, $query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $atts['perpage'] );

	ob_start();
	if ($query->have_posts()) : ?>

<div class="article-grid grid grid-1 grid-lg-<?php echo $atts['cols'];?> gap-3">
  <?php while ($query->have_posts()) : 
				$query->the_post(); 
				$ID = get_the_ID();
				$index = $query->current_post; 
				$postURL = get_permalink(); ?>

  <div class="article-grid__article">
    <?php
						$imageSize = 'article-grid'; 
						$image = get_the_post_thumbnail($ID, $imageSize, ['class' => 'rounded']);
						echo sprintf("<a class='article-grid__image-wrapper' href='%s'>%s</a>", $postURL, $image); ?>
    <span class="article-grid__date"><?php the_time('F jS, Y'); ?></span>
    <h3 class="article-grid__title">
      <a href="<?php echo $postURL; ?>">
        <?php the_title(); ?>
      </a>
    </h3>
    <a href="<?php echo $postURL; ?>" class="article-grid__icon-link">
      <i class="fa fa-arrow-right text-white"></i>
    </a>
  </div>

  <?php endwhile; ?>
</div>

<?php if ($pagination) : ?>
<div class="pagination">
  <?php echo paginate_links( array(
					'total'   => $total_pages,
					'current' => $current_page,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
				) ); ?>
</div>
<?php endif; 
		// pagination($total_pages) 
		?>

<?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
});

// Echo either Yogi Bear or Boo Boo, Yogi: [echo-field field="content"] | Boo Boo: [echo-field acf_field="anything"]
// More Yogi [echo-field field="post_thumbnail" size="thumbnail"] // params are useful for image sizes
add_shortcode('echo-field', function($atts) {
    if (is_admin()) {return null;}

    extract(shortcode_atts(array(
        'field' => '',
        'acf_field' => '',
        'params' => ''
    ), $atts));

    $ID = get_the_ID();

    $output = false;

    if (!empty($acf_field)) {
        // Check to ensure boo boo is available
        if (function_exists('get_field')) {
            $output = get_field($acf_field);
        }
    }

    // Yogi takes presidence -- make sure field is not set if acf_field is
    if (!empty($field)) {
        $output = 'get_the_' . $field; 
    } 

    // There are inconsistencies with the amount of params in each wordpress function so here is where you adjust 
    // as needed on a case by case... E.G ID needs to come first when doing custom image sizes, but not others
    if ($output && function_exists($output)) {
        switch($output) {
            case 'get_the_post_thumbnail':
                $output = $output($ID, $params);
            break;
            case 'get_the_ID':
                $output = $ID;
            break; 
            default:
                $output = $output();
        }
    }

    return $output ?: 'There were no fields matching your query';
});


// This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
add_shortcode('related-posts', function($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts( [
		"perpage" => 6,
		"offset" => 0,
		"cols" => 3,
		"gap" => 0,
		"cat" => null,
		"post_type" => 'post',
    "paged" => true,
    "text" => 'light'
	], $atts );
	
	if (!get_query_var('categories') && isset($atts['cat'])) {
		$categoryIDs = $atts['cat']; 
	}
	
	$args = [
		"posts_per_page" => $atts['perpage'],
		"post_type" => $atts['post_type'],
    "post_status" => "publish",
    "post__not_in" => array(get_the_ID()),
		"category__in" => $categoryIDs ? [$categoryIDs] : ''
  ];
  
  // Headers white or black
  $color = $atts['text'] === "light" ? 'white' : 'black';
  $opposite_color = $atts['text'] !== "light" ? 'black' : 'white';

	
	$query = new WP_Query($args); 

	ob_start();
  if ($query->have_posts()) : ?>


<div class="section container">
  <div class="article-grid article-grid__related-posts grid">
    <?php while ($query->have_posts()) :       
        $query->the_post();
        $index = $query->current_post; 
        ?>

    <div class="col-<?php echo 12 / intval($atts['cols']);?>">
      <?php include(locate_template("template-parts/article-condensed.php")); ?>
    </div>

    <?php endwhile; ?>

  </div>
</div>

<?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
});
