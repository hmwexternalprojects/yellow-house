<?php

// [menu name=""]
add_shortcode('menu', function($atts, $content = null) {
	extract(shortcode_atts(array( 'name' => null, ), $atts));
	$content = '<div class="menu-toggle"><span></span><span></span><span></span></div>';
	$content .= wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
	return $content;
});

add_shortcode('logo', function() {
  if ( is_admin() ){ return null; }

  if ( function_exists( 'the_custom_logo' )) {
    
    // From theme customizer -- Vertical Logo
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); 
    
    ob_start(); ?>

<a href="<?php echo get_site_url(); ?>">
  <?php if ($custom_logo_id) : ?>
  <img src="<?php echo esc_attr($image[0]); ?>" alt="Website Logo">
  <?php else : ?>
  <h2 class="site-logo-text mb-0"><?php echo do_shortcode( '[sitename]' ); ?></h2>
  <?php endif; ?>
</a>

<?php 
    return ob_get_clean();
   }
  
});

//[searchform]
add_shortcode('searchform', function() {
	return get_search_form(false);
});

// [phone]
add_shortcode('company-phone', function() {
    if ( is_admin() ){ return null; }

    $company_phone = 'xxxx xxx xxx';

    if (function_exists('get_field')) {
      $company_phone = get_field('company_phone', 'option') ? get_field('company_phone', 'option') : $company_phone;
    } 

    ob_start(); ?>

<a class="company-phone"
  href="<?php echo esc_attr( 'tel:+61' . str_replace('+61', '', preg_replace('/\D/', '', $company_phone))); ?>">
  <span><?php esc_attr_e( $company_phone, 'hmw-starter-child' ); ?></span>
</a>

<?php 
    return ob_get_clean();
});

add_shortcode('company-fax', function() {
  if ( is_admin() ){ return null; }

  $company_fax = 'xxxx xxx xxx';

  if (function_exists('get_field')) {
    $company_fax = get_field('company_fax', 'option') ? get_field('company_fax', 'option') : $company_fax;
  } 

  ob_start(); ?>

<a class="company-fax" href="<?php echo esc_attr( 'fax:' . $company_fax ); ?>">
  <span><?php esc_attr_e( $company_fax, 'hmw-starter-child' ); ?></span>
</a>

<?php 
  return ob_get_clean();
});

// [email]
add_shortcode('company-email', function() {
    if ( is_admin() || !function_exists('get_field') ){ return null; }

    $company_email = get_field('company_email', 'option') ? get_field('company_email', 'option') : '';

    ob_start(); ?>

<a class="company-email" href="mailto:<?php echo esc_attr( $company_email ); ?>">
  <span><?php echo esc_attr_e( $company_email, 'hmw-starter-child' ); ?></span>
</a>
<?php 
    return ob_get_clean();
});

// [year]
add_shortcode('year', function() {
    if ( is_admin() ){ return null; }
    return date('Y');
});

// [sitename]
add_shortcode('sitename', function() {
    if ( is_admin() ){ return null; }
    return get_option( 'blogname' );
});

// [home_url]
add_shortcode('home_url', function() {
    if ( is_admin() ){ return null; }
    return esc_url( home_url( '/' ) );
});

// [credits]
add_shortcode('credits', function($atts, $content = null) {
    if ( is_admin() ){ return null; }

    extract(shortcode_atts(array( 'logo' => false, ), $atts));
    $domain_name = preg_replace('/^www\./','',$_SERVER['SERVER_NAME']);
    if( $logo == 'true' ) {
        $html = 'Website by <a href="https://www.handmadeweb.com.au/?utm_source=client_footer&utm_medium=referral&utm_campaign='. $domain_name .'" rel="nofollow" target="_blank"><img src="'. get_stylesheet_directory_uri() .'/images/hmw-logo-white.png" alt="Handmade Web & Design" /></a>';
    } else {
    	$html = 'Website by <a href="https://www.handmadeweb.com.au/?utm_source=client_footer&utm_medium=referral&utm_campaign='. $domain_name .'" rel="nofollow" target="_blank">Handmade Web & Design</a>';
    }
    return $html;
});

// Font awesome social icons for the footer
add_shortcode('social-icons', function() {   
  
  ob_start();    
  
  if( have_rows('social_links', 'option') ): ?>

<ul class="fa-social-icons flex items-center mb-0">

  <?php while( have_rows('social_links', 'option') ): the_row(); ?>

  <li class="fa-social-icon flex items-center px-1"><a href="<?php the_sub_field('link'); ?>"
      class="text-lg fa fab fa-<?php echo strtolower(get_sub_field('network')); ?>"></a></li>

  <?php endwhile; ?>

</ul>

<?php endif;
    
  return ob_get_clean();

});

// [share_post]
add_shortcode('share_post', function($atts) {
    extract(shortcode_atts(array(
    'title' => 'Share'
    ), $atts));
    ob_start();
    if( is_single() || function_exists('is_product') && is_product() ) {
    $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full'); ?>
<div class="social-share">
  <span class="share-title"><?php echo $title; ?></span>
  <ul>
    <li class="facebook fab fa-facebook"><a
        href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"></a></li>
    <li class="twitter fab fa-twitter"><a href="https://twitter.com/home?status=<?php echo get_permalink(); ?>"
        target="_blank"></a></li>
    <li class="pinterest fab fa-pinterest"><a
        href="https://pinterest.com/pin/create/button/?url=&media=<?php echo $img[0]; ?>&description="
        target="_blank"></a></li>
    <li class="email fas fa-envelope"><a href="mailto:?&body=<?php echo get_permalink(); ?>"></a></li>
  </ul>
</div>
<?php }
    return ob_get_clean();
});