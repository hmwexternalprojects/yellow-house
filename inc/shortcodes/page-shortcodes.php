<?php 

add_shortcode('list-child-pages', function() {
    if(is_admin()){ return null; }

    $ID = get_the_ID();

    $args = array(
        'post_parent' => $ID,
        'numberposts' => -1,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC'
    );

    $children = get_children( $args );

    if (get_field('additional_icon_blocks', $ID)) {
        foreach(get_field('additional_icon_blocks') as $icon_block) {
            array_push($children, $icon_block);
        }
    }
    
    ob_start();    
    
    ?>

<ul class="child-page-list">

  <?php if ($children) : foreach($children as $child) : ?>

  <li class="child-page hover-grow-sm">
    <div class="child-page__top">
      <?php the_post_thumnail(); ?>
      <?php if (get_field('page_icon', $child->ID)) : ?>
      <i
        class="icon-before icon-<?php echo get_field('page_icon', $child->ID) ? get_field('page_icon', $child->ID) : 'fa-marker'; ?>"></i>
      <?php endif; ?>
    </div>
    <div class="child-page__body">
      <a href=""><?php echo $child->post_title; ?></span></a>
    </div>
  </li>

  <?php endforeach; endif; ?>

</ul>

<?php
    
    $output = ob_get_clean();
    
    return $output;
});

// For the buildy feature "Internal Link"
add_shortcode('page-content-links', function() {
	return '<div id="page-content-links"></div>';
});

add_shortcode('terms-conditions-text', function() {
  return get_field('terms_conditions_text', 'options');
});