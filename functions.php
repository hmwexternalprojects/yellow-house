<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


wp_enqueue_editor();

function hmw_remove_parent_stuff() {

    wp_dequeue_style( 'hmw-frontend-styles' );
    wp_deregister_style( 'hmw-frontend-styles' );

    wp_dequeue_script( 'hmw-frontend-scripts' );
    wp_deregister_script( 'hmw-frontend-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'hmw_remove_parent_stuff', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
  // Enqueue Fonts (if any)
  options_google_font_enqueue();

  wp_enqueue_script( 'sweet-alert', 'https://unpkg.com/sweetalert2@8.8.1/dist/sweetalert2.all.min.js', ['jquery'], null, true );
  
  // Get the theme data
  // $the_theme = wp_get_theme();
  $frontEndScriptDependancies = ['jquery', 'sweet-alert'];
  if (function_exists('is_woocommerce_activated')) {
    array_push($frontEndScriptDependancies, 'woocommerce');
  }
  wp_register_script( 'hmw-child-frontend-scripts', get_stylesheet_directory_uri() . '/public/frontend-bundle.js', $frontEndScriptDependancies, null, true );
	wp_localize_script( 'hmw-child-frontend-scripts', 'global_vars', array(
    'admin_ajax_url' => admin_url( 'admin-ajax.php' ),
    'site_url' => get_bloginfo('siteurl'),
		'ajax_url' => admin_url('admin-ajax.php'),
		'ajax_nonce' => wp_create_nonce('ajax-nonce'),
		'popup_status' => $_COOKIE['popup_status']
  ) );
  

	// Loads bundled frontend CSS.
	wp_enqueue_style( 'hmw-theme-options', get_stylesheet_directory_uri() . '/public/hmw-theme-options.css' );	
  wp_enqueue_script('hmw-child-frontend-scripts');
  
}

// Enqueue main CSS with lower priorty to easily override woocommerce etc
add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'hmw-child-frontend-styles', get_stylesheet_directory_uri() . '/public/frontend.css' );	
}, 15 );

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

/****************************************************************************************************************
* If any of these files don't exist it will revert to parent themes version
*/
include get_theme_file_path( '/inc/shortcodes/default-shortcodes.php' ); // These are the default shortcodes we bundle 
include get_theme_file_path( '/inc/shortcodes/post-shortcodes.php' ); // Everything related to posts
include get_theme_file_path( '/inc/shortcodes/page-shortcodes.php' ); // Everything related to pages

/****************************************************************************************************************
* Add custom image sizes
*/

add_image_size('collection-large', 1010, 1036, true);
add_image_size('collection-small', 1010, 360, true);

/**
 * Get all the registered image sizes along with their dimensions
 *
 * @global array $_wp_additional_image_sizes
 *
 * @link http://core.trac.wordpress.org/ticket/18947 Reference ticket
 * @return array $image_sizes The image sizes
 */
function _get_all_image_sizes() {
	global $_wp_additional_image_sizes;

	$default_image_sizes = array( 'thumbnail', 'medium', 'large' );
	 
	foreach ( $default_image_sizes as $size ) {
		$image_sizes[$size]['width']	= intval( get_option( "{$size}_size_w") );
		$image_sizes[$size]['height'] = intval( get_option( "{$size}_size_h") );
		$image_sizes[$size]['crop']	= get_option( "{$size}_crop" ) ? get_option( "{$size}_crop" ) : false;
	}
	
	if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) )
		$image_sizes = array_merge( $image_sizes, $_wp_additional_image_sizes );
		
	return $image_sizes;
}

/**
 * Enqueue google fonts (if set in site options)
 */
function options_google_font_enqueue() {
  $subsets = array();
  $font_element = array();
  
  $font_main = get_field('font_main', 'option');
  $font_headings = get_field('font_headings', 'option');
  $font_buttons = get_field('font_buttons', 'option'); 

  $font_array = [];

  if ($font_main) : 
    if (!is_array($font_array[$font_main['font']])) {
      $font_array[$font_main['font']] = $font_main;
    } else {
      if (is_array($font_main['variants'])) {
        $font_array[$font_main['font']]['variants'] = array_merge($font_array[$font_main['font']]['variants'], $font_main['variants']);
      }
    }
  endif;
  if ($font_headings) :
    if (!is_array($font_array[$font_headings['font']])) {
      $font_array[$font_headings['font']] = $font_headings;
    } else {
      if (is_array($font_headings['variants'])) {
        $font_array[$font_headings['font']]['variants'] = array_merge($font_array[$font_headings['font']]['variants'], $font_headings['variants']);
      }
    }
  endif;
  if ($font_buttons) : 
    if (!is_array($font_array[$font_buttons['font']])) {
      $font_array[$font_buttons['font']] = $font_buttons;
    } else {
      if (is_array($font_buttons['variants'])) {
        $font_array[$font_buttons['font']]['variants'] = array_merge($font_array[$font_buttons['font']]['variants'], $font_buttons['variants']);
      }
    }
  endif;

  foreach($font_array as $key=>$font) :
    if (is_array($font['subsets'] )) {
      $subsets = array_merge( $subsets, $font['subsets'] );
    }
    $font_name = $font['font'];
    if ($font['variants'] == array('regular') || !$font['variants'] ) {
      $font_element[] = $font_name;
    } else {
        if (is_array($font['variants'])) {
          $regular_variant = array_search( 'regular', $font['variants'] );
        }
        if( $regular_variant !== false ) {
            $font['variants'][$regular_variant] = '400';
        }
        $font_element[] = $font_name . ':' . implode( ',', $font['variants'] );
      }
  endforeach;

  // check if the repeater field has rows of data
  if( have_rows('additional_fonts', 'option') ):
    
    while ( have_rows('additional_fonts', 'option') ) : the_row(); 
      $font = get_sub_field('font_selector');
      if (is_array($font['subsets'] )) {
        $subsets = array_merge( $subsets, $font['subsets'] );
      }
      $font_name = str_replace( ' ', '+', $font['font'] );
      if( $font['variants'] == array( 'regular' ) || !$font['variants'] ) {
          $font_element[] = $font_name;
      }
      else {
        if (is_array($font['variants'])) {
          $regular_variant = array_search( 'regular', $font['variants'] );
        }
        if( $regular_variant !== false ) {
            $font['variants'][$regular_variant] = '400';
        }
        $font_element[] = $font_name . ':' . implode( ',', $font['variants'] );
      }
    endwhile; 
  endif;  

  $subsets = ( empty( $subsets ) ) ? array('latin') : array_unique( $subsets );

  $subset_string = implode( ',', $subsets );

  $font_string = implode( '|', array_unique($font_element));

  $request = '//fonts.googleapis.com/css?family=' . $font_string . '&subset=' . $subset_string;

  wp_enqueue_style( 'acfgfs-enqueue-fonts', $request );
}

// Populate colour options
// Add the global colour options to all of these fields
add_filter('acf/load_field/key=field_5ee954e6b2ab6', 'add_site_color_choices');
add_filter('acf/load_field/key=field_5eeaef43ae7d2', 'add_site_color_choices');
add_filter('acf/load_field/key=field_5eeaef41ae7d1', 'add_site_color_choices');
add_filter('acf/load_field/key=field_5eeb424893807', 'add_site_color_choices');

function add_site_color_choices($field) {
  // reset choices
  $field['choices'] = array();

  // get the textarea value from options page without any formatting
  // check if the repeater field has rows of data
  if( have_rows('theme_colours', 'option') ):

      // loop through the rows of data
      while ( have_rows('theme_colours', 'option') ) : the_row();

        $colour = trim(get_sub_field('name'));

        $field['choices'][ 'None' ] = 'None';
        $field['choices'][ $colour ] = $colour;

      endwhile; 
  endif; 

  // return the field
  return $field;
}

function render_common_globals() {
  global $buildy;
  ob_start();
    echo $buildy->renderFrontend('248'); 
    echo $buildy->renderFrontend('245'); 
    echo $buildy->renderFrontend('96'); 
  return ob_get_clean();
}

// Search Results Page
add_action( 'before_search_results', function() {
  ob_start(); 
  global $wp_query;
  ?>


<!-- Search Section Start -->
<div class="bmcb-section search">
  <!-- Search Row Start -->
  <div class="bmcb-row container">
    <!-- Col Start -->
    <div class="col-12">
      <div class="search-results">
        <div class="search-results__count">
          <?php 
        $term = get_search_query();
        $resultsFound = $wp_query->found_posts > 0;
        _e(($resultsFound ? "Showing {$resultsFound} - {$wp_query->found_posts} for {$term}" : "We found 0 results for {$term}"), 'yellowhouse'); ?>
        </div>
        <div class="search-results__form">
          <?php get_search_form(); ?>
        </div>
      </div>
      <?php 
echo ob_get_clean();
}, 5 ); 

add_action( 'after_search_results', function() {
  ob_start(); ?>
    </div> <!-- Col End -->
  </div> <!-- Search Row End -->
</div> <!-- Search Section End -->
<?php 
    echo ob_get_clean();
});

add_action('after_search_content', function() {
  echo render_common_globals();
});


add_filter('single_template', 'check_for_category_single_template');
function check_for_category_single_template( $t )
{
  foreach( (array) get_the_category() as $cat ) 
  { 
    if ( file_exists(STYLESHEETPATH . "/single-cat-{$cat->slug}.php") ) return STYLESHEETPATH . "/single-cat-{$cat->slug}.php"; 
    if($cat->parent)
    {
      $cat = get_the_category_by_ID( $cat->parent );
      if ( file_exists(STYLESHEETPATH . "/single-cat-{$cat->slug}.php") ) return STYLESHEETPATH . "/single-cat-{$cat->slug}.php";
    }
  } 
  return $t;
}

// Popup - AJAX - Remove w/ Cookie
function remove_location_popup() {
	// Check for nonce security
  $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) ){
		die();
  }
	// Set Cookie
	$popup_status = $_POST['popup_status'];
	setcookie( 'popup_status', $popup_status, '', '/' );
  echo json_encode( $popup_status );
  // Die
  die();
}

add_action('wp_ajax_ajax_remove_popup', 'remove_location_popup');
add_action('wp_ajax_nopriv_ajax_remove_popup', 'remove_location_popup');