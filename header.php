<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">

  <script>
  window.MSInputMethodContext && document.documentMode && document.write(
    '<script src="https://cdn.jsdelivr.net/gh/nuxodin/ie11CustomProperties@4.0.1/ie11CustomProperties.min.js"><\x2fscript>'
  );
  </script>


  <?php wp_head(); ?>
</head>

<?php 
	$builderClass = '';
	if (function_exists('get_field')) {
		if (get_field('page_builder_enabled', get_the_ID())) {
			$builderClass = 'page-builder-enabled';
		}
	}
?>

<body <?php body_class($builderClass); ?>>
  <button class="menu-toggle" id="menu-toggle" aria-expanded="false"><span class="screen-reader-text">Menu</span>
    <svg class="icon icon-menu-toggle" aria-hidden="true" version="1.1" xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100">
      <g class="svg-menu-toggle">
        <path class="line line-1" d="M5 13h90v14H5z" />
        <path class="line line-2" d="M5 43h90v14H5z" />
        <path class="line line-3" d="M5 73h90v14H5z" />
      </g>
    </svg>
  </button>
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hmw' ); ?></a>
  <div id="page" class="site">
    <?php if (get_field('enable_top_header', 'option')) : ?>
    <div id="top-header">
      <div class="container">
        <div id="secondary-menu">
          <?php
						echo wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'top-header__menu', 'menu_id' => 'top-menu', 'echo' => false ) );
					?>
        </div> <!-- #secondary-menu -->
        <div class="header-phone-wrapper flex items-center">
          <i class="fas text-link fa-mobile-alt pr-1"></i>
          <?php echo do_shortcode( '[company-phone]' ); ?>
        </div>
      </div> <!-- .comtainer -->
    </div> <!-- #top-header -->
    <?php endif; ?>
    
     <?php if (get_field('enable_header_notice', 'option') && $_COOKIE['popup_status'] !== 'disabled') : ?>
      <div id="header-notice" class="header-notice alert alert-light alert-dismissible fade show" role="alert">
        <div class="container">
          <?php echo get_field('header_notice', 'option'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div> <!-- .container -->
      </div> <!-- .header-notice -->
    <?php endif; ?>

    <?php 
    if (get_field('main_header_background_colour', 'option')) : 
      $header_colour = get_field('main_header_background_colour', 'option');
    endif;
  ?>
    <header id="main-header"
      <?php echo (isset($header_colour) && $header_colour !== 'None') ? "class='bg-{$header_colour}'" : null;  ?>>
      <div class="container clearfix">
        <div class="logo_container">
          <?php echo do_shortcode( '[logo]' ); ?>
        </div>

        <div id="main-nav">
          <nav id="top-menu-nav">
            <?php
								echo wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'main-menu-nav', 'menu_id' => 'main-menu', 'echo' => false ) );
							?>
            <?php hmw_woocommerce_header_cart(); ?>
            <div id="header-search__toggle" class="header-search__toggle">
              <img src="/wp-content/uploads/2020/07/search-icon.svg">
            </div>
          </nav>
          <div id="header-search__form" class="header-search__form">
            <?php get_search_form(); ?>
          </div>
        </div> <!-- #main-nav -->

      </div> <!-- .container -->
    </header> <!-- #main-header -->

    <?php if (!is_front_page()) : ?>
    <div class="page-title-bar">
      <div class="container">
        <h1 class="page-title-bar__title">
          <?php if (is_woocommerce()) : 
              woocommerce_page_title(); 
            else :
              wp_title(''); 
            endif;
          ?>
        </h1>
      </div>
    </div>
    <?php endif; ?>
