import spinner from '../components/spinner'

export default {
  init() {
  },
  finalize() {
    // Terms & Conditions thing
    let termsState = {
      clickHanderActive: false,
      termsText: ''
    }

    jQuery('body').on('updated_checkout', function () {
      let termsPopup = document.getElementById('terms-popup')
      let checkbox = document.getElementById('terms')

      if (!termsState.clickHanderActive) {
        termsPopup.addEventListener('click', async function (e) {
          termsState.clickHanderActive = true;
          let oldText = e.target.innerHTML
          e.preventDefault();

          if (!termsState.termsText) {
            // Add Loader
            e.target.parentNode.classList.add('loading')
            e.target.innerHTML = spinner;

            // Get Terms Text
            let res = await fetch('/wp-json/acf/v3/options/options'),
                data = await res.json();
                
            termsState.termsText = data.acf.terms_conditions_text

            // Remove Loader
            e.target.innerHTML = oldText;
            e.target.parentNode.classList.remove('loading')
          }

          // Open Terms Popup
          swal.fire({
            title: 'Terms and conditions',
            html: termsState.termsText,
            type: 'info',
            confirmButtonText: `Accept`,
            confirmButtonColor: 'var(--color-bravo)',
            cancelButtonColor: 'var(--color-lightgray)',
            showCancelButton: true,
            cancelButtonText: 'Decline',
            buttonsStyling: false,
            padding: '2rem',
            width: '60rem',
            preConfirm: () => checkbox.checked = true,
          }).then(function (res) {
            // Check if it was canceled
            if (
              res.dismiss === Swal.DismissReason.cancel
            ) {
              // If they declined, uncheck the checkbox
              checkbox.checked = false
            }
          });
        })
      }
    });
  },
};
