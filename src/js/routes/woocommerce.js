import { isAbove } from '../util/breakpoints'

export default {
  init() {
    (function () {
      let top_level_SF = document.querySelectorAll('.searchandfilter > ul > li');
      let handlersActive = []

      if (!isAbove.lg.matches) {
        enabledSFToggleMenus()
      }

      isAbove.lg.addListener(function (e) {
        if (!e.matches) {
          enabledSFToggleMenus()
        } else {
          disableSFToggleMenus(e)
        }
      })

      function enabledSFToggleMenus() {
        top_level_SF.forEach((el, i) => {
          let header = el.querySelector('h4');
          if (!handlersActive.includes(header)) {
            header.addEventListener('click', setupToggle)
          }
        })
      }

      function setupToggle(e) {
        handlersActive.push(e.target)
        let parent = e.target.parentNode
        parent.classList.toggle('is-open')
      }

      function disableSFToggleMenus() {
        top_level_SF.forEach((el, i) => {
          let header = el.querySelector('h4');
          header.removeEventListener('click', setupToggle)
        })
        handlersActive = []
      }

    })()
  },
  finalize() {

  }
};
