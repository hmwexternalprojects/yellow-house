import stickyBody from '../components/stickyBody'
import { testiSlider } from "../components/testimonialSlider"
import mainMenu from '../components/mainMenu'
import postFilter from '../components/postFilter'
import accordions from '../components/accordion'
import searchToggle from '../components/searchToggle'

export default {
  init() {
    // Main Menu
    mainMenu();

    // Search Toggle
    searchToggle();

    testiSlider();

    // Accordions
    accordions();

    stickyBody('#main-header');
  },
  finalize() {
    // testiSlider();

    // Set loading spinner on ajax button click
    (function () {
      let clickHandler = false
      if (jQuery('.products').length && jQuery('.ajax_add_to_cart').length) {

        jQuery(document).on("sf:ajaxfinish", ".searchandfilter", function () {
          setupCartClicks()
        });

        setupCartClicks()

        function setupCartClicks() {
          if (clickHandler === false) {
            jQuery('.ajax_add_to_cart').on('click', function () {
              let btnText = jQuery(this).text();
              let spinner = `<svg data-buttontxt="${btnText}" version="1.1" id="loading_spinner" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50" transform="rotate(295.42 50 50)">
                <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>
                </path>
              </svg>`;
              jQuery(this).html(spinner);
            })
            clickHandler = true
          }
        }
      }
    })();

    // Handle the add to cart alert
    jQuery(document.body).on('added_to_cart', function (e, fragments, cart_hash, this_button) {
      let loadingSpinner = jQuery(this_button).find('#loading_spinner')
      let buttonText = loadingSpinner.data('buttontxt')

      jQuery.ajax({
        type: 'POST',
        url: wc_add_to_cart_params.ajax_url,
        data: {
          'action': 'ajax_added_to_cart_items',
          'added': 'yes',
          'product_id': this_button[0].dataset.product_id
        },
        success: function (response) {
          response = JSON.parse(response)
          let product = response.product;

          swal.fire({
            title: `${product.name} was added to your cart`,
            type: 'success',
            confirmButtonText: `View Cart`,
            confirmButtonColor: 'var(--color-bravo)',
            cancelButtonColor: 'var(--color-lightgray)',
            showCancelButton: true,
            cancelButtonText: 'Continue Shopping',
            buttonsStyling: false,
            padding: '2rem',
            width: '60rem',
            preConfirm: () => {
              return window.location.href = response.cart_url
            },
          });

          if (loadingSpinner) {
            jQuery(this_button).text(buttonText)
            loadingSpinner.remove();
          }


        },
        error: function (err) {
          if (loadingSpinner) {
            jQuery(this_button).text(buttonText)
            loadingSpinner.remove();
          }
          throw new Error('There was an issue adding that product to your cart', err)
        }
      });
    });
  },
};
