export default function() {
  let formLabels = document.querySelectorAll('.gfield_label');

  if (!formLabels) {
    return false;
  }

  formLabels.forEach(label => {
    let origTransform = label.style.transform;
    let target = label.getAttribute('for');
    let input = document.getElementById(target);
    if (input) {
      input.addEventListener('focus', () => moveLabelUp(label));
      // Keydown is ran only once, this fixes a bug where you have a small chance
      // of clicking into an input before the JS has loaded.
      input.addEventListener('keydown', () => moveLabelUp(label), {
        once: true
      });
      input.addEventListener('blur', () =>
        moveLabelDown(label, input, origTransform)
      );
    }
  });

  function moveLabelUp(label) {
    label.style.transform = 'none';
  }

  function moveLabelDown(label, input, origTransform) {
    if (input.value === '') {
      label.style.transform = origTransform;
    }
  }
}
