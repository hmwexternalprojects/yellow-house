import ContentToggler from './classes/contentToggler'
import { isAbove } from '../util/breakpoints'

export default function () {

  let headerSearch = new ContentToggler('#header-search__toggle', '#header-search__form');

  headerSearch.toggleButton.addEventListener('click', function (e) {
    e.stopPropagation();
    if (!headerSearch.isOpen) {
      headerSearch.open()
      document.addEventListener('click', clickOutside);
    } else {
      headerSearch.close()
      document.removeEventListener('click', clickOutside);
    }
  })

  if (!isAbove.lg.matches) {
    headerSearch.open();
  }

  isAbove.lg.addListener((e) => {
    if (e.matches) {
      headerSearch.close();
    } else {
      headerSearch.open();
      document.removeEventListener('click', clickOutside);
    }
  })


  function clickOutside(event) {
    var isClickInside = headerSearch.toggleContent.contains(event.target);

    if (!isClickInside && headerSearch.isOpen) {
      headerSearch.close();
    }
  }
}
