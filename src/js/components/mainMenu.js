import debounce from '../util/debounce';

export default function () {
  let $ = jQuery;
  let windowWidth = window.innerWidth,
    mainNav = $('#main-nav'),
    body = $('body'),
    mobileMenuActive = false,
    originalHrefs = [];

  if (windowWidth < 980 && !mobileMenuActive) {
    setupMobileMenu()
  }

  $(window).on('resize', debounce(() => {
    windowWidth = window.innerWidth;
    if (windowWidth < 980) {
      if (!mobileMenuActive) {
        setupMobileMenu()
      }
    } else {
      if (mobileMenuActive) {
        destroyMobileMenu()
      }
    }
  }))

  function setupMobileMenu() {
    mobileMenuActive = true;
    body.addClass('has-mobile-menu')

    body.prepend('<div class="mobile-menu" />')
    let mobileContainer = $('.mobile-menu')
    let mobileToggle = $('#menu-toggle');

    // Remember original positions
    mainNav.data('originalParent', mainNav.parent())

    // Move to mobile nav
    mobileContainer.append(mainNav)

    mobileToggle[0].addEventListener('click', toggleVisibility)

    setup_collapsible_submenus();
  }

  function closeOnOutsideClick(e) {
    let $target = $(e.target);
    if (!$target.closest('.mobile-menu').length) {
      toggleVisibility(e)
    }
  }

  function toggleVisibility(e) {
    e.stopPropagation();
    if (body.hasClass('mobile-menu-is-visible')) {
      body.removeClass('mobile-menu-is-visible');
      $(this).attr('aria-expanded', 'false');
      document.removeEventListener('click', closeOnOutsideClick, { once: true })
    } else {
      body.addClass('mobile-menu-is-visible');
      $(this).attr('aria-expanded', 'true');
      document.addEventListener('click', closeOnOutsideClick, { once: true })
    }
  }

  function destroyMobileMenu() {
    mobileMenuActive = false;

    let mobileToggle = $('#menu-toggle');

    mobileToggle[0].removeEventListener('click', toggleVisibility)


    body.removeClass('has-mobile-menu')
      .removeClass('mobile-menu-is-visible')

    mainNav.appendTo(mainNav.data('originalParent'))

    $('.mobile-menu').remove();
    remove_collapsible_submenus();
  }

  // This will allow tapping to open nested menus -- Only way for mobile I could think of
  function setup_collapsible_submenus() {
    mainNav.find('a').each(function (i) {
      $(this).off('click');

      // If this a tag contains a 
      if ($(this).siblings('.sub-menu').length) {
        // Since we are removing the href, we'll store the original (by index) in an array for later
        originalHrefs.push({
          index: i,
          url: $(this).attr('href'),
        })

        // Remove href
        $(this).attr('href', '#');
      }
      // Otherwise if it has one of these siblings it needs to toggle visible on them
      if ($(this).siblings('.sub-menu').length) {
        $(this).on('click', function (event) {
          event.preventDefault();
          if ($(this).siblings('.sub-menu').length) {
            $(this).parent().toggleClass('is-open')
            $(this).siblings('.sub-menu').toggleClass('is-visible')
              .find('.sub-menu')
              .toggleClass('is-visible');
          }
        });
      }
    });
  }

  // Reset collapsible states back to normal (so regular clicks work with hover state)
  function remove_collapsible_submenus() {
    // Remove any open items from
    $('.is-visible').removeClass('is-visible');
    mainNav.find('a').each(function (i) {
      $(this).off('click');
      $(this).find('i').remove();

      if ($(this).siblings('.sub-menu').length) {

        let originalHref = originalHrefs.find(function (el) {
          return el.index === i
        })

        $(this).attr('href', originalHref.url);
      }

    });
  }

}
