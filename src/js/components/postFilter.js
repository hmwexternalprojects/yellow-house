import { filter } from "minimatch";

export default function(){
    let $ = jQuery;
    let filterControls = $('.filter-controls');
    let catFilter = filterControls.find('.filter-controls__category-filter');

    if (filterControls.length) {
        catFilter.on('change', function() {
            filterControls.submit();
        })
    }
};
