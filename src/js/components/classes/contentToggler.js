/* 
  A universal content toggler to create anything that needs an element to show/hide
  The implementation (click, hover) is up to you
  @module ContentToggle
  @param1 String = Button selector, the thing you want to click to open
  @param2 String = Content selector, the thing you want to open when button is clicked
*/

export default class ContentToggler {
  constructor(button, content) {
    this.toggleButton = document.querySelector(button)
    this.toggleContent = document.querySelector(content)
    this.isOpen = false;
  }

  open() {
    if (!this.toggleContent.classList.contains('is-open'))
      this.toggleContent.classList.add('is-open')
    this.isOpen = true
  }

  close() {
    if (this.toggleContent.classList.contains('is-open'))
      this.toggleContent.classList.remove('is-open')
    this.isOpen = false
  }
}
