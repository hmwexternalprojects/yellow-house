import Siema from 'siema';

let sliderClass = '.testimonial-slider'
let testiSlider = e => e

if (document.querySelector(sliderClass)) {
  
  
  testiSlider = () => {
    let currentIndex;
    let boxContent = document.querySelectorAll('.testimonial-slider__content')
    
    const prev = document.querySelectorAll('.arrow-left');
    const next = document.querySelectorAll('.arrow-right');
    
    const testiSlider = new Siema({
      selector: sliderClass,
      duration: 200,
      easing: 'ease-out',
      perPage: 1,
      startIndex: 0,
      draggable: true,
      multipleDrag: true,
      threshold: 20,
      loop: true,
      rtl: false,
      onInit: function() {
        currentIndex = this.currentSlide
        boxContent[this.currentSlide].classList.add('is-visible')
      },
      onChange: function() {
        boxContent.forEach(el => el.classList.replace("is-visible", "is-hidden"))

        let pagination = this.selector.querySelector('.slider__navigation-dots');
        let isActive = pagination.querySelector('.is-active');
       
        // Remove isActive from any other ones
        if (isActive) {
          isActive.classList.remove('is-active')
        }

        // Make the clicked one active
        pagination.children[this.currentSlide].classList.add('is-active');
        
        if (boxContent[this.currentSlide].classList.contains('is-hidden')) {
          boxContent[this.currentSlide].classList.replace('is-hidden', 'is-visible')
        }
      },
    });

    testiSlider.addPagination();
    
    if (next && prev) {
      prev.forEach((el) => el.addEventListener('click', () => testiSlider.prev()))
      next.forEach((el) => el.addEventListener('click', () => testiSlider.next()))
    }
  }
  
}

Siema.prototype.addPagination = function() {
  const ul = document.createElement('ul');
  ul.classList.add('slider__navigation-dots');
  for (let i = 0; i < this.innerElements.length; i++) {
    const li = document.createElement('li');
    // li.textContent = i;

    if (i === 0) {
      li.classList.add('is-active')
    }

    li.addEventListener('click', (el) => {
      this.goTo(i)
    });

    ul.appendChild(li)
  }
  this.selector.appendChild(ul);

}

export {testiSlider}
