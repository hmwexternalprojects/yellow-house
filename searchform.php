<?php
/**
 * The searchform.php template.
 *
 * Used any time that get_search_form() is called.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

/*
 * Generate a unique ID for each form and a string containing an aria-label
 * if one was passed to get_search_form() in the args array.
 */

$aria_label = ! empty( $args['label'] ) ? 'aria-label="' . esc_attr( $args['label'] ) . '"' : '';
?>
<form role="search"
  <?php echo $aria_label; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- Escaped above. ?>
  method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <label class="flex items-center bg-foxtrot" for="<?php echo esc_attr( $unique_id ); ?>">
    <span
      class="screen-reader-text"><?php _e( 'Search for:', 'hmw' ); // phpcs:ignore: WordPress.Security.EscapeOutput.UnsafePrintingFunction -- core trusts translations ?></span>
    <span><i class="fa fa-search"></i></span>
    <input type="search" id="<?php echo esc_attr( $unique_id ); ?>" class="search-field"
      placeholder="<?php echo esc_attr_x( 'Search by plant type &hellip;', 'placeholder', 'hmw' ); ?>"
      value="<?php echo get_search_query(); ?>" name="s" />
    <button type="submit" class="search-submit">Search</button>
  </label>
</form>
