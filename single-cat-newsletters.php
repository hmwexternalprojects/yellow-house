<?php
 get_header();
?>

<div <?php post_class(); ?> id="main-content">
	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<div class="bmcb-section container ">
				<div class="bmcb-row row ">
					<div class="bmcb-column col col-lg-4 ">
						<div class="bmcb-text-module bmcb-module entry-content">
							<?php
								$ID = get_the_ID();
								$intro = get_field('introduction', $ID) ? get_field('introduction', $ID) : false;

							if ($intro) {
								echo "<p id='post-intro' class='font-large'>$intro</p>";
							} ?>
							<?php the_post_thumbnail( 'medium' ) ?>   
						</div>
					</div>
					<div class="bmcb-column col col-lg-8 sidebar">
						<?php the_content(); ?>
					</div>
				</div>
			</div>

		</div>

	<?php endwhile; ?>

</div> <!-- #main-content -->

<?php

echo render_common_globals();
get_footer();
