<?php

/* Template Name: Coming Soon */

?>

<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">

  <?php wp_head(); ?>
</head>

<?php 
	$builderClass = '';
	if (function_exists('get_field')) {
		if (get_field('page_builder_enabled', get_the_ID())) {
			$builderClass = 'page-builder-enabled';
		}
	}
?>

<body <?php body_class($builderClass); ?>>
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hmw' ); ?></a>
  <div id="page" class="site">
    <header class="coming-soon__header">
      <div class="container clearfix flex items-center justify-center">
        <div class="logo_container">
          <?php echo do_shortcode( '[logo]' ); ?>
        </div>
      </div> <!-- .container -->
    </header> <!-- #main-header -->

    <div <?php post_class(); ?> id="main-content">

      <?php while ( have_posts() ) : the_post(); ?>

      <div class="content-wrap">

        <?php
				the_content();

				if ( ! $is_page_builder_used )
					wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
			?>

      </div>

      <?php endwhile; ?>
    </div> <!-- #main-content -->